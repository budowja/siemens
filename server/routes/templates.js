/*
    Handle all URL routing. Currently only handles GET, can be configured
    to handle all requests, including POSTS, DELETES, etc.
*/

import express from 'express';

import { getTemplates } from '../controllers/templates.js'

const router = express.Router();

// Execute on visiting localhost:5000/templates
router.get('/', getTemplates);

export default router;