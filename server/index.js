import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import templateRoutes from './routes/templates.js';
import TemplatePost from './models/templatePost.js';
import { templateData } from './data/templates.js';
import { extendedTemplatesData } from './data/extendedTemplates.js';

// Create Express App Connection
const app = express();
app.use(cors());

// Create route to localhost:5000/templates
app.use('/templates', templateRoutes);

// https://wwww.mongodb.com/cloud/atlas
// Using Mongo on Server Side to host DB
// Connection URL and Port are left public and out of .env file for this project since
// the mongoDB user is a read only user, so that anyone who downloads and runs this
// project can still view all files. For admin access however, it is privatized.
const PORT = 5000;
const CONNECTION_URL = 'mongodb+srv://generaluser:XYHXZA6iqLAsZasB@cluster0.2drrt.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';

// Create connection to MongoDB on port 5000
mongoose.connect(CONNECTION_URL, {})
    .then(() => app.listen(PORT, () => console.log(`Server running on port: ${PORT}`)))
    .catch((error) => console.log(error.message));

// DB Admin Functions, will not work for normal users. For controlling the data from server side.
// TemplatePost.remove( function ( err ) {
//       console.log('success');
// });
//TemplatePost.insertMany(templateData);
// TemplatePost.insertMany(extendedTemplatesData);
