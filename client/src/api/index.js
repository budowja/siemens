import axios from 'axios'; // For calls to backend

// Communicate with the backend, by querying the API via URL
const url = 'http://localhost:5000/templates';

export const fetchTemplates = () => axios.get(url);