import { FETCH_ALL } from '../constants/actionTypes';
import * as api from '../api';

// Action Creators
// Async for live updates since data is being fetched from MongoDB Atlas
// dispatch is parameterized since it is passed into the methods body
export const getTemplates = () => async (dispatch) =>{
    try {
        const { data } = await api.fetchTemplates();

        // Dispatch() calls the reducer
        // type: signals to switch case what to execute
        // payload: signals all data being passed
        dispatch({ type: FETCH_ALL, payload: data });
    } catch (error) {
        console.log(error.message);
    }
}