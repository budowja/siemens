import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import './styles.css';

const Templates = (props) => {
    // Get array from DB of all thumbnails and store it in templates constant
    const templatesOrigin = useSelector((state) => state.templates);

    // Remove any duplicate IDs from the array, since we do not care about showing the same template multiple times
    const templates = Array.from(new Set(templatesOrigin.map(a => a.id)))
        .map(id => {
            return templatesOrigin.find(a => a.id === id)
        })

    // Create state hook for the currentID, with subState as the activeID. This is a string.
    const [currentID, changeID] = useState({
        activeID: '',
    });

    // Create state hook for page. This is numeric.
    const [page, changePage] = useState(1);

    // On thumbnail click, change currentID.activeID state to the id of the thumbnail chosen.
    // this informs toggleActiveStyles() as to which anchor should be set to active
    // On thumbnail click, pass the selected thumbnails information to the parent, App.js, using props
    function toggleActive(template) {
        changeID({ ...currentID, activeID: template.id });
        props.func(template);
    }

    // Toggle active style on selected thumbnail
    // called from anchors className
    function toggleActiveStyles(id) {
        if (id === currentID.activeID) {
            return "active";
        } else {
            return "inactive";
        }
    }

    // In order to make the app pre load, do pagination based on display types. Therefore, all data from templates is
    // loaded in, but only display thumbnails based on page and index, using basic math logic. This is called from the anchors style={{}}
    function toggleDisplayStyles(index) {
        if (index < page * 4 && index >= 4 * (page - 1)) {
            return { display: 'block' };
        } else {
            return { display: 'none' };
        }
    }

    // If not the first page of thumbnails, decrease page by 1
    // called from onClick
    function paginatePrevious() {
        if (page !== 1)
            changePage(page - 1);
    }

    // If not the last page of thumbnails, increase page by 1
    // called from onClick.
    function paginateNext() {
        if (page !== Math.ceil(templates.length / 4))
            changePage(page + 1);
    }

    // if page state variable is displaying end of templates array, disable button CSS, else enable button
    function enableDisableNext() {
        if (page === Math.ceil(templates.length / 4)) {
            return "next disabled";
        } else {
            return "next";
        }
    }

    // if page state variable is displaying beginning of templates array, disable button CSS, else enable button
    function enableDisablePrevious() {
        if (page === 1) {
            return "previous disabled";
        } else {
            return "previous";
        }
    }

    /* 
        If statement:
        Should the DB somehow get emptied, rather than displaying nothing, display a message to contact the DB Admin.
        If DB is not empty, then display the thumbnails as intended.
    */
    if (templates.length === 0) {

        return (
            <p>It appears the Database is empty! Contact <strong>budowja@gmail.com</strong> about this error.</p>
        )

    } else {

        return (
            <div className="thumbnails">
                <div className="group">
                    {
                        templates.map((template, index) => (

                            <a
                                key={index}
                                href="/templates"
                                title={template.id}
                                style={toggleDisplayStyles(index)}
                                className={toggleActiveStyles(template.id)}
                                onClick={(event) => {
                                    event.preventDefault();
                                    toggleActive(template);
                                }}
                            >
                                <img src={'/images/thumbnails/' + template.thumbnail} alt={template.id + '-m'} width="145" height="121" />
                                <span>{(template.id).substring(0, 4)}</span>
                            </a>

                        ))
                    }
                    <span className={enableDisablePrevious()} title="Previous" onClick={() => {
                        paginatePrevious();
                    }}>Previous</span>
                    <span className={enableDisableNext()} title="Next" onClick={() => {
                        paginateNext();
                    }}>Next</span>
                </div>
            </div>
        );

    }
};

export default Templates;