import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import reducers from './reducers';

import App from './App';

// The application can function without the use of Redux if the code is heavily modified
// Thunk here is used to streamline asynchronous actions. This is especially nice if we were to
// make the template viewer update the list of thumbnails live on changes, rather than waiting for
// a refresh.

// React dispatches to Action -> Thunk then contacts API and sends information to Reducer -> This is placed in the store -> payload passed back to React.
const store = createStore(reducers, compose(applyMiddleware(thunk)));

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);