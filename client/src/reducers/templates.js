import { FETCH_ALL } from "../constants/actionTypes";

// ./Action dispatches payload/type to function, ie. switch case
// action parameter is all dispatched information (type, payload)
// templates initialized to empty array
const templates = (templates = [], action) => {
    switch (action.type) {
        case FETCH_ALL:
            return action.payload;
        default:
            return templates;
    }
}

export default templates;