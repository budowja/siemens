// Implemented for Redux scalability. In the event multiple reducers are present in code at scale
// combine all reducers in one file to reduce access points in other files

import { combineReducers } from 'redux';

import templates from './templates';

export default combineReducers({ templates });