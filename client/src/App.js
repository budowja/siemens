import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { getTemplates } from './actions/templates'
import Templates from './components/Templates/Templates';

import './styles.css';

const App = () => {
    // Redux
    const dispatch = useDispatch();

    // State hook for template data, since when the page loads we want it to have empty populated parameters to avoid any errors
    // caused by attempting to load templateData.data.* with nothing defined.
    const [templateData, changeTemplateData] = useState({
        data: {
            title: '',
            description: '',
            cost: '',
            id: '',
            thumbnail: '',
            image: '',
        },
    })

    // Redux, after render
    useEffect(() => {
        dispatch(getTemplates());
    }, [dispatch]);

    // Function to pull the selected template from Templates.js to App.js
    const pull_data = (data) => {

        // Update the templateData state variable with the template information
        changeTemplateData({ data });
    }

    return (
        <div id="container">
            <header>Code Development Project</header>
            <div id="main" role="main">
                <div id="large">
                    <div className="group">
                        {(templateData.data.id === '' && <p>Please select a template...</p>) ||
                            <img src={'/images/large/' + templateData.data.image} alt="Large" width="430" height="360" />}
                        <div className="details">
                            <p><strong>Title</strong> {templateData.data.title}</p>
                            <p><strong>Description</strong> {templateData.data.description}</p>
                            <p><strong>Cost</strong> ${templateData.data.cost}</p>
                            <p><strong>ID #</strong> {templateData.data.id}</p>
                            <p><strong>Thumbnail File</strong> {templateData.data.thumbnail}</p>
                            <p><strong>Large Image File</strong> {templateData.data.image}</p>
                        </div>
                    </div>
                </div>
                <Templates
                    func={pull_data}
                />
            </div>
            <footer>
                <p>Made by Jack Budow for Siemens</p>
            </footer>
        </div>
    );
};

export default App;