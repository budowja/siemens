# Informational Wiki About Project #

[ Wiki ](https://bitbucket.org/budowja/siemens/wiki/Home)

# How to Install and Deploy #

In order to run npm commands, you must have nodejs installed on your PC. Download that here and install it if you don't have it:

[ Download Node.js ](https://nodejs.org/en/download/)

### Installation ###

Clone the respository using command line:

```git clone https://budowja@bitbucket.org/budowja/siemens.git```

Once you have cloned the repo, open two terminals. Navigate to ```siemens/client``` and ```siemens/server``` using the ```cd``` command.

In each terminal, type:

```npm i```

This will install all the packages needed to run the application using NPM, the node packet manager.

### Run the Application ###

Once you have installed all packages in both client and server, type:

```npm start```

In ```siemens/server``` this will run the backend of the application on port 5000. In ```siemens/client``` this will run the front end on port 3000.

If your browser does not automatically open to the URL http://localhost:3000/ then navigate there yourself. The application should be fully operational.